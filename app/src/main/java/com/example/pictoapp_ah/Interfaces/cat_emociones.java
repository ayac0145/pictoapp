package com.example.pictoapp_ah.Interfaces;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.widget.GridView;
import com.example.pictoapp_ah.BD.baseContract;
import androidx.fragment.app.Fragment;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.Interfaces.Pictogramas;
import com.example.pictoapp_ah.Interfaces.PictogramasListAdapter;
import com.example.pictoapp_ah.R;

import java.util.ArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
/**
 * A simple {@link Fragment} subclass.
 */
public class cat_emociones extends Fragment {

    GridView gridView;
    ArrayList<Pictogramas> list;
    PictogramasListAdapter adapter=null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.lugares_list_activity, container, false);


        gridView=(GridView)view.findViewById(R.id.gridView);
        list=new ArrayList<>();
        adapter=new PictogramasListAdapter(getActivity().getApplicationContext(), R.layout.pictogramas_items, list);
        gridView.setAdapter(adapter);


        //Obtener Datos
        PictogramasBdHepler bd= new PictogramasBdHepler(getActivity().getApplicationContext(), "pictogramas", null, 1);
        Cursor cursor=bd.consultaPicto(4);
        list.clear();
        cursor.moveToFirst();
        while (cursor.moveToNext())
        {
            String nom=cursor.getString(0);
            String cat="Emociones";
            String subCat=cursor.getString(3);
            int col_img=cursor.getColumnIndex(baseContract.PictogramasEntry.IMAGEN);
            byte[] image=cursor.getBlob(col_img);
            list.add(new Pictogramas(nom, cat, subCat, image));
        }
        adapter.notifyDataSetChanged();
        return view;
    }

    public interface OnFragmentInteractionListener{
        void OnFragmentInteraction(Uri uri);

    }

}