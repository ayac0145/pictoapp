package com.example.pictoapp_ah.Interfaces.Controlador;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.R;

import java.io.ByteArrayInputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class Borrar extends Fragment {
    ImageView imageView;
    Button buttonBorrar;
    EditText editTextNom;
    ImageButton imageButtonBusqueda;
    PictogramasBdHepler conn;

    public Borrar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        conn = new PictogramasBdHepler(getActivity().getApplicationContext(),"pictogramas",null,1);

        View view = inflater.inflate(R.layout.fragment_borrar, container, false);

        imageView = (ImageView) view.findViewById(R.id.imageViewBorrar);
        buttonBorrar= (Button) view.findViewById(R.id.btnBorrar);
        editTextNom = (EditText) view.findViewById(R.id.nombreBorrar);
        imageButtonBusqueda = (ImageButton) view.findViewById(R.id.imageButtonBorrar);
        buttonBorrar.setEnabled(false);

        //Metodo para que el imageView cambie con el pictograma buscado
        imageButtonBusqueda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte [] imgRecuperada;
                Cursor cursor = conn.consultaPictoNombre(editTextNom.getText().toString());
                if(cursor.moveToFirst()){
                    imgRecuperada = cursor.getBlob(0);
                    ByteArrayInputStream bais = new ByteArrayInputStream(imgRecuperada);
                    Bitmap bitmap = BitmapFactory.decodeStream(bais);

                    imageView.setImageBitmap(bitmap);
                    buttonBorrar.setEnabled(true);
                }

            }
        });

        //Relizado para que se borre el pictograma mostradp
        buttonBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextNom.getText().toString().equals(" ")){
                    Toast.makeText(getActivity().getApplicationContext(),"No hay pictograma seleccionado para borrar",Toast.LENGTH_LONG).show();
                }else{
                   showDeleteDialog(editTextNom.getText().toString());
                }

            }
        });


        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.busqueda));
        editTextNom.setText("");
        buttonBorrar.setEnabled(false);

    }

    public interface OnFragmentInteractionListener{
        void OnFragmentInteraction(Uri uri);
    }
    //Metodo usado para pedir confimacion de borrado
    public void showDeleteDialog(final String Nom){
        AlertDialog.Builder dialogDelete = new AlertDialog.Builder(getActivity());

        dialogDelete.setTitle("ALERTA!!");
        dialogDelete.setMessage("¿Esta seguro de que desea borrar el pictograma?");
        dialogDelete.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                conn.borrarPicto(Nom);
                Toast.makeText(getActivity().getApplicationContext(),"Pictograma Eliminado",Toast.LENGTH_LONG).show();
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.busqueda));
                editTextNom.setText("");
                buttonBorrar.setEnabled(false);
            }
        });
        dialogDelete.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }

}
