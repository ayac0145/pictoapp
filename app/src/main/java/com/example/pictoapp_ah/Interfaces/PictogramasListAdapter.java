package com.example.pictoapp_ah.Interfaces;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pictoapp_ah.R;

import java.util.ArrayList;

public class PictogramasListAdapter extends BaseAdapter
{

    private Context context;
    private int layout;
    private ArrayList<Pictogramas> pictogramasList;

    public PictogramasListAdapter(Context context, int layout, ArrayList<Pictogramas> pictogramasList) {
        this.context = context;
        this.layout = layout;
        this.pictogramasList = pictogramasList;
    }

    @Override
    public int getCount() {
        return pictogramasList.size();
    }

    @Override
    public Object getItem(int position) {
        return pictogramasList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private  class  ViewHolder
    {
        ImageView imageView;
        TextView txtName, txtCategoria, txtSubcategora;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup)
    {
        View row=view;
        ViewHolder holder=new ViewHolder();
        if(row==null)
        {
            LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(layout,null);
            holder.txtName=(TextView)row.findViewById(R.id.txtnombre);
            holder.txtCategoria=(TextView)row.findViewById(R.id.txtcategoria);
            holder.txtSubcategora=(TextView)row.findViewById(R.id.txtsubcategoria);
            holder.imageView=(ImageView)row.findViewById(R.id.imgPicto2);
            row.setTag(holder);
        }
        else
        {
            holder=(ViewHolder)row.getTag();
        }
        Pictogramas pictogramas=pictogramasList.get(position);
        holder.txtName.setText(pictogramas.getNombre());
        holder.txtCategoria.setText(""+pictogramas.getId_categoria());
        holder.txtSubcategora.setText(pictogramas.getNombre_subcategoria());

        byte[] pictoImage=pictogramas.getPictograma();
        Bitmap bitmap= BitmapFactory.decodeByteArray(pictoImage,0,pictoImage.length);
        holder.imageView.setImageBitmap(bitmap);
        return row;
    }
}
