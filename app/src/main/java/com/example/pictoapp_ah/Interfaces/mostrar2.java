package com.example.pictoapp_ah.Interfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.R;

import java.io.ByteArrayInputStream;

public class mostrar2 extends AppCompatActivity {

    ImageView image;
    ImageButton imgbtn;
    TextView name;
    TextView  categoria, subcategoria;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar2);
        image=(ImageView)findViewById(R.id.image);
        name=(TextView) findViewById(R.id.name);
        categoria=(TextView)findViewById(R.id.categoria);
        categoria.setText("NO");
        subcategoria=(TextView)findViewById(R.id.subcategoria);
        imgbtn=(ImageButton)findViewById(R.id.imgbtnS);
        byte [] imgRecuperada;
        imgRecuperada=getIntent().getByteArrayExtra("imagen");
        ByteArrayInputStream ios = new ByteArrayInputStream(imgRecuperada);
        Bitmap imagen = BitmapFactory.decodeStream(ios);
        image.setImageBitmap(imagen);

    //name.setText(getIntent().getStringExtra("nombre"));

        PictogramasBdHepler bd=new PictogramasBdHepler(this, "pictogramas", null, 1);
        Cursor cursor_con =bd.consultaPictoAtribNombre(getIntent().getStringExtra("nombre"));
        cursor_con.moveToFirst();
        name.setText("Nombre: "+cursor_con.getString(0));
        categoria.setText("Categoria: "+bd.consultaCategorias(cursor_con.getLong(1)));
        if(cursor_con.getString(3)==null) {
            subcategoria.setText("");
        } else {
            subcategoria.setText("Subcategoria: "+cursor_con.getString(3));
        }
        /*Cursor consulta=bd.consultaPictoAtribNombre(name.getText().toString());
        if(consulta.moveToFirst())
        {
            String temp="";
            temp+="Columna 0 "+consulta.getColumnName(0)+consulta.getString(0)+"\n";
            temp+="Columna 1 "+consulta.getColumnName(1)+consulta.getInt(1)+"\n";
            temp+="Columna 2 "+consulta.getColumnName(2)+consulta.getInt(2)+"\n";
            temp+="Columna 3 "+consulta.getColumnName(3)+consulta.getInt(3)+"\n";
            temp+="Columna 4 "+consulta.getColumnName(4);

            subcategoria.setText(temp);
        }*/

        imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent bus_palabra=new Intent(getApplicationContext(), bus_palabra.class);
                startActivity(bus_palabra);
            }
        });
    }
}
