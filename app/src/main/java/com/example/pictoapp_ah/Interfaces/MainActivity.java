package com.example.pictoapp_ah.Interfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText itxt_pwd;
    Button btnadmin;
    PictogramasBdHepler helper = new PictogramasBdHepler (this, "pictogramas", null, 1);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        itxt_pwd =(EditText) findViewById(R.id.itxt_passwd);
        btnadmin = findViewById(R.id.btnAdmin);
        findViewById(R.id.btnAdmin).setOnClickListener(this);
        findViewById(R.id.btnAcceder).setOnClickListener(this);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.btnAdmin:
                if(itxt_pwd.getVisibility()==View.INVISIBLE){
                    btnadmin.setBackgroundResource(R.drawable.button_pulsado);
                    itxt_pwd.setVisibility(view.VISIBLE);
                }else{
                    itxt_pwd.setVisibility(View.INVISIBLE);
                    btnadmin.setBackgroundResource(R.drawable.button_rounded_border);
                }
            break;
            case R.id.btnAcceder:
                if(itxt_pwd.getVisibility()==View.VISIBLE){
                    try {
                        Cursor cursor = helper.consultaContra(itxt_pwd.getText().toString());  // Ocupo el ConsultaContra para determinar el acceso al menu de administrador
                        if (cursor.getCount() > 0) {
                            itxt_pwd.setVisibility(view.INVISIBLE);
                            itxt_pwd.setText("");
                            Intent i = new Intent(getApplicationContext(), AdministradorActivity.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(getApplicationContext(), "CONTRASEÑA INCORRECTA",
                                    Toast.LENGTH_LONG).show();
                        }
                    }catch(SQLException e){
                        e.printStackTrace();
                    }
                }
                else
                {
                    Intent tipo_busqueda= new Intent(getApplicationContext(), seleccion_busqueda.class);
                    startActivity(tipo_busqueda);
                }
             break;
        }

    }
}
