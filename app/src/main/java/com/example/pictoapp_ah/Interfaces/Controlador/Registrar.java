package com.example.pictoapp_ah.Interfaces.Controlador;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.BD.baseContract;
import com.example.pictoapp_ah.R;
import com.example.pictoapp_ah.Utilidades.Categorias;
import com.example.pictoapp_ah.Utilidades.Subcategorias;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.CAMERA_SERVICE;

public class Registrar extends Fragment {
    //Spinner Categorias
    Spinner Combocategorias;
    ArrayList<String> listaCategorias;
    ArrayList<Categorias> CategoriasList;

    //Spiiner Subcategorias
    Spinner ComboSubcategorias;
    ArrayList<String> listaSubCategorias;
    ArrayList<Subcategorias> SubCategoriasList;

    PictogramasBdHepler conn;
    Uri imageUri;
    Button btnOk;
    private OnFragmentInteractionListener aListener;
    private static final int PICK_IMAGE=100;
    ImageView imagen;
    EditText editText;

    public Registrar() {
        // Required empty public constructor

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        conn = new PictogramasBdHepler(getActivity().getApplicationContext(),"pictogramas",null,1);

        View view = inflater.inflate(R.layout.fragment_registrar, container, false);
        Combocategorias = (Spinner)view.findViewById(R.id.ListCategorias);
        ComboSubcategorias = (Spinner)view.findViewById(R.id.ListSubCategorias);
        imagen= (ImageView)view.findViewById(R.id.busqImg);
        btnOk=view.findViewById(R.id.btnGuardar);
        editText = view.findViewById(R.id.tv1);
        imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long idCat=0, idSubCat=0;
                String subCat="";
                String SelectCat = Combocategorias.getSelectedItem().toString();
                Cursor cursorCat = conn.consultaCategorias(SelectCat);
                if(cursorCat.moveToFirst()){
                    idCat= cursorCat.getInt(cursorCat.getColumnIndex("id_categoria"));
                    if(ComboSubcategorias.getSelectedItem().toString().equals("Sin subcategorias disponibles")){//Se realizan comparaciones del Spinner de Subcategorias para asegurarse de que si cuenta con alguna
                        subCat="No aplica"; //Subcat pasa a se No Aplica para evitar confuciones
                    }else{
                        subCat=ComboSubcategorias.getSelectedItem().toString();
                        idSubCat=idCat;//Si tiene un nombre diferente a "Sin subcategorias disponibles, idSubcat toma el valor de idCat, en caso contrario queda como 0
                    }
                    try{
                        Cursor picto = conn.consultaPictoNombre(editText.getText().toString());
                        if(picto.moveToFirst()){
                            Toast.makeText(getActivity().getApplicationContext(),"Ya existe un pictograma con ese nombre!",Toast.LENGTH_LONG).show();
                        }else {
                            conn.insertPicto(
                                    editText.getText().toString().trim(),
                                    idCat,
                                    idSubCat,
                                    subCat,
                                    imageViewToByte(imagen));
                            editText.setText("");
                            Combocategorias.setSelection(0);
                        }

                    }catch (Exception e){e.printStackTrace();}

            }}
        });
        consultarCategorias();

        //Spinner Categorias
        ArrayAdapter<CharSequence> adaptador = new ArrayAdapter(getActivity().getApplicationContext(),android.R.layout.simple_spinner_item,listaCategorias);
        adaptador.setDropDownViewResource(R.layout.list_item_registrar);
        Combocategorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // Permite saber la posicion del Spinner con el fin de evitar que la app colapse
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                String item_position = String.valueOf(position);
                int positonInt = Integer.valueOf(item_position);
                // Obtiene la posicion para ignorar el primero de "Seleccione una categoria" y que no mande error
                if(positonInt>0){
                    consultaSubCategorias();
                    //Spinner Subcategorias
                    ArrayAdapter<CharSequence> adaptador2 = new ArrayAdapter(getActivity().getApplicationContext(),android.R.layout.simple_spinner_item,listaSubCategorias);
                    adaptador2.setDropDownViewResource(R.layout.list_item_registrar);
                    ComboSubcategorias.setAdapter(adaptador2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        Combocategorias.setAdapter(adaptador);
        return view;
    }
    private byte[]imageViewToByte(ImageView  imagen){
        Bitmap bitmap = ((BitmapDrawable) imagen.getDrawable()).getBitmap();
        ByteArrayOutputStream stream= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,0,stream);
        byte[] imagenByte= stream.toByteArray();
        return imagenByte;
    }

    private void consultaSubCategorias() {
        String SelectCat = Combocategorias.getSelectedItem().toString();
        Cursor cursorCat = conn.consultaCategorias(SelectCat); //Retorna el ID de las Catergorias
        if(cursorCat !=null && cursorCat.moveToFirst()){
            Subcategorias subCat=null;
            SubCategoriasList = new ArrayList<Subcategorias>();
            Cursor cursorSub = conn.consultaSubCategorias(cursorCat.getInt(cursorCat.getColumnIndex("id_categoria")));
            if( cursorSub.moveToFirst()){//Se realiza esto para que si no cuenta con Subcategorias no mande nada
                cursorSub.moveToPrevious();
                while(cursorSub.moveToNext()){
                    subCat = new Subcategorias();

                    subCat.setID(cursorSub.getInt(0));
                    subCat.setNombre(cursorSub.getString(1));

                    Log.i("id_categoria",subCat.getId().toString());
                    Log.i("nombre",subCat.getNombre());

                    SubCategoriasList.add(subCat);
                }
                obtenerListaSubcategorias(1);

            }else{
                obtenerListaSubcategorias(2);

            }
        }
    }
    private void obtenerListaSubcategorias(int n) {
        listaSubCategorias = new ArrayList<String>();
        if (n==1){
             listaSubCategorias.add("Seleccione una Subcategoria");
            System.out.println(SubCategoriasList.size());
            for(int i=0; i<SubCategoriasList.size();i++){
                listaSubCategorias.add(SubCategoriasList.get(i).getNombre());
            }
        } else if(n==2){
            listaSubCategorias.add("Sin subcategorias disponibles");

        }
    }

    private void consultarCategorias() {
        Categorias cat=null;
        CategoriasList = new ArrayList<Categorias>();
        Cursor cursor = conn.consultaCategorias();
        while(cursor.moveToNext()){
             cat= new Categorias();

             cat.setID(cursor.getInt(0));
             cat.setNombre(cursor.getString(1));

             Log.i("id_categoria",cat.getId().toString());
             Log.i("nombre",cat.getNombre());

             CategoriasList.add(cat);
        }
        obtenerLista();
    }
    private void obtenerLista() {
        listaCategorias= new ArrayList<String>();
        listaCategorias.add("Seleccione Categoria");
        for(int i=0;i<CategoriasList.size();i++){
            listaCategorias.add(CategoriasList.get(i).getNombre());
            System.out.println(listaCategorias);
        }
    }
    public interface OnFragmentInteractionListener{
        void OnFragmentInteraction(Uri uri);
    }

    private void openGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent,PICK_IMAGE);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
           imageUri = data.getData();
           imagen.setImageURI(imageUri);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==101){
            if(grantResults.length==2 && grantResults[0]==PackageManager.PERMISSION_GRANTED
                && grantResults[1]==PackageManager.PERMISSION_GRANTED){
                btnOk.setEnabled(true);
            }
        }
    }
}
