package com.example.pictoapp_ah.Interfaces;

public class Pictogramas
{
    String nombre;
    String id_categoria;
    int id_categoria_sub;
    String nombre_subcategoria;
    byte[] pictograma;

    public Pictogramas(String nombre, String id_categoria, int id_categoria_sub, String nombre_subcategoria, byte[] pictograma) {
        this.nombre = nombre;
        this.id_categoria = id_categoria;
        this.id_categoria_sub = id_categoria_sub;
        this.nombre_subcategoria = nombre_subcategoria;
        this.pictograma = pictograma;
    }

    public Pictogramas(String nombre, String id_categoria, String nombre_subcategoria, byte[] pictograma) {
        this.nombre = nombre;
        this.id_categoria = id_categoria;
        this.nombre_subcategoria = nombre_subcategoria;
        this.pictograma = pictograma;
    }

    public Pictogramas(String nombre, String id_categoria, byte[] pictograma) {
        this.nombre = nombre;
        this.id_categoria = id_categoria;
        this.pictograma = pictograma;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(String id_categoria) {
        this.id_categoria = id_categoria;
    }

    public int getId_categoria_sub() {
        return id_categoria_sub;
    }

    public void setId_categoria_sub(int id_categoria_sub) {
        this.id_categoria_sub = id_categoria_sub;
    }

    public String getNombre_subcategoria() {
        return nombre_subcategoria;
    }

    public void setNombre_subcategoria(String nombre_subcategoria) {
        this.nombre_subcategoria = nombre_subcategoria;
    }

    public byte[] getPictograma() {
        return pictograma;
    }

    public void setPictograma(byte[] pictograma) {
        this.pictograma = pictograma;
    }
}
