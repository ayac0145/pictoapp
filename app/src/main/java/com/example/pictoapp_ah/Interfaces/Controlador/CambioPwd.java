package com.example.pictoapp_ah.Interfaces.Controlador;


import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.Interfaces.AdministradorActivity;
import com.example.pictoapp_ah.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CambioPwd extends Fragment {
    EditText editTextpwdO, editTextpwdN,editTextpwdRN;
    Button btnpwd;
    PictogramasBdHepler conn;
    public CambioPwd() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        conn = new PictogramasBdHepler(getActivity().getApplicationContext(),"pictogramas",null,1);
        View view = inflater.inflate(R.layout.fragment_cambio_pwd, container, false);

        editTextpwdO = (EditText)view.findViewById(R.id.editTextPwdOld);
        editTextpwdN = (EditText)view.findViewById(R.id.editTextPwdNew);
        editTextpwdRN = (EditText)view.findViewById(R.id.editTextPwdRnew);
        btnpwd = (Button) view.findViewById(R.id.btnCambioPwd);

        btnpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarCampos()){
                    conn.updateContra(editTextpwdN.getText().toString());
                    editTextpwdN.setText("");
                    editTextpwdRN.setText("");
                    editTextpwdO.setText("");
                    Toast.makeText(getActivity().getApplicationContext(), "CONSTRASEÑA ACTUALIZADA", Toast.LENGTH_LONG).show();
                }
            }


        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        editTextpwdN.setText("");
        editTextpwdRN.setText("");
        editTextpwdO.setText("");
    }

    public interface OnFragmentInteractionListener{
        void OnFragmentInteraction(Uri uri);

    }
    private boolean validarCampos() {
        try {
            Cursor cursor = conn.consultaContra(editTextpwdO.getText().toString());  // Ocupo el ConsultaContra para determinar el acceso al menu de administrador
            if(cursor.getCount() > 0) {
                if(editTextpwdN.getText().toString().matches("") ){
                    Toast.makeText(getActivity().getApplicationContext(), "CAMPO CONSTRASEÑA NUEVA VACIO", Toast.LENGTH_LONG).show();
                    return false;
                }if(editTextpwdRN.getText().toString().matches("")){
                    Toast.makeText(getActivity().getApplicationContext(), "CAMPO REPETIR CONSTRASEÑA NUEVA VACIO", Toast.LENGTH_LONG).show();
                    return false;
                }if(!editTextpwdN.getText().toString().equals(editTextpwdRN.getText().toString())){
                    System.out.println(editTextpwdN.getText().toString());
                    System.out.println(editTextpwdRN.getText().toString());

                    Toast.makeText(getActivity().getApplicationContext(), "CAMPO CONSTRASEÑA NUEVA Y REPETIR CONSTRASEÑA NUEVA NO COINCIDEN", Toast.LENGTH_LONG).show();
                    return false;
                }if(conn.consultaContra((editTextpwdN.getText().toString())).getCount()>0){
                    Toast.makeText(getActivity().getApplicationContext(), "LA CONSTRASEÑA NUEVA NO PUEDE SER IGUAL QUE LA ANTIGUA", Toast.LENGTH_LONG).show();
                    return false;
                }
                else{
                    return true;
                }
            }else {
                Toast.makeText(getActivity().getApplicationContext(), "CONTRASEÑA ACTUAL INCORRECTA", Toast.LENGTH_LONG).show();
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
}
