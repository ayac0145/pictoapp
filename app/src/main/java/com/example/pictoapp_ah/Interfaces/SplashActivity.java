package com.example.pictoapp_ah.Interfaces;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.R;

import java.io.ByteArrayOutputStream;

public class SplashActivity extends AppCompatActivity {
    PictogramasBdHepler conn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        conn = new PictogramasBdHepler(getApplicationContext(),"pictogramas",null,1);
        Cursor cursor = conn.consultaAllPicto();
        if(cursor.getCount()>0){
            System.out.println("CON datos ");
        }else{
            System.out.println("Insertando datos ");
            //Insertar Acciones
            Drawable image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.logo);
            conn.insertPicto("Accion",5,imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.besar);
            conn.insertPicto("Besar",5,imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.saludar);
            conn.insertPicto("Saludar",5,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.secar);
            conn.insertPicto("Secar",5,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.lavar_platos);
            conn.insertPicto("Lavar platos",5,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.lavar_manos);
            conn.insertPicto("Lavar manos",5,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.lavar_cara);
            conn.insertPicto("Lavar cara",5,imageViewToByte(image));

            //Insertar Cuerpo
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.logo);
            conn.insertPicto("Cuerpos",6,imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.axila);
            conn.insertPicto("Axila",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.pie);
            conn.insertPicto("Pie",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.barbilla);
            conn.insertPicto("Barbilla",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.brazo);
            conn.insertPicto("Brazo",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.cara);
            conn.insertPicto("Cara",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.ceja);
            conn.insertPicto("Ceja",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.codo);
            conn.insertPicto("Codo",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.cuello);
            conn.insertPicto("Cuello",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.dedomano);
            conn.insertPicto("Dedos de la Mano",6,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.dedospie);
            conn.insertPicto("Dedos del Pie",6,imageViewToByte(image));

            //Insertar Emociones
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.logo);
            conn.insertPicto("Emocion",4,imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.cansado);
            conn.insertPicto("Cansado",4,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.confundido);
            conn.insertPicto("Confundido",4,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.enojado);
            conn.insertPicto("Enojado",4,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.feliz);
            conn.insertPicto("Feliz",4,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.llorar);
            conn.insertPicto("Llorar",4,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.sorprendido);
            conn.insertPicto("Sorprendido",4,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.triste);
            conn.insertPicto("Triste",4,imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.dolor);
            conn.insertPicto("Dolor",4,imageViewToByte(image));

            //Insertar Comida
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.logo);
            conn.insertPicto("Comi",3,imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.agua);
            conn.insertPicto("Agua",3, "Bebidas",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.refresco);
            conn.insertPicto("Refresco",3, "Bebidas",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.vino);
            conn.insertPicto("Vino",3, "Bebidas",imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.burrito);
            conn.insertPicto("Burrito",3, "Comida rápida",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.pizza);
            conn.insertPicto("Pizza",3, "Comida rápida",imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.manzana);
            conn.insertPicto("Manzana",3, "Frutas",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.pera);
            conn.insertPicto("Pera",3, "Frutas",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.platano);
            conn.insertPicto("Platano",3, "Frutas",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.platanos);
            conn.insertPicto("Platanos",3, "Frutas",imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.dona);
            conn.insertPicto("Dona",3, "Postres",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.helado);
            conn.insertPicto("Helado",3, "Postres",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.pastel);
            conn.insertPicto("Pastel",3, "Postres",imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.apio);
            conn.insertPicto("Apio",3, "Verduras",imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.cebolla);
            conn.insertPicto("Cebolla",3, "Verduras",imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.zanahoria);
            conn.insertPicto("Zanahoria",3, "Verduras",imageViewToByte(image));


            //lugares
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.logo);
            conn.insertPicto("lugar",2,imageViewToByte(image));

            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.banio);
            conn.insertPicto("Baño",2,"Interior",
                    imageViewToByte(image));
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.cocina);
            conn.insertPicto("Cocina",2,"Interior",
                    imageViewToByte(image));

            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.jugeteria);
            conn.insertPicto("Jugueteria", 2,"Exterior",imageViewToByte(image));
            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.tienda);
            conn.insertPicto("Tienda", 2,"Exterior",imageViewToByte(image));


            //Familia
            image = ContextCompat.getDrawable(getApplicationContext(),R.drawable.logo);
            conn.insertPicto("fami",1,imageViewToByte(image));

            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.mama);
            conn.insertPicto("Mama", 1,imageViewToByte(image));

            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.papa);
            conn.insertPicto("Papa", 1,imageViewToByte(image));
            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.abuela);
            conn.insertPicto("Abuela", 1,imageViewToByte(image));
            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.abuelo);
            conn.insertPicto("Abuelo", 1,imageViewToByte(image));
            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.abuelos);
            conn.insertPicto("Abuelos", 1,imageViewToByte(image));
            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.primas);
            conn.insertPicto("Primas", 1,imageViewToByte(image));
            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.primos);
            conn.insertPicto("Primos", 1,imageViewToByte(image));
            image=ContextCompat.getDrawable(getApplicationContext(),R.drawable.hermano);
            conn.insertPicto("Hermano", 1,imageViewToByte(image));

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },2000);
    }
    private byte[]imageViewToByte(Drawable imagen){
        Bitmap bitmap = ((BitmapDrawable)imagen).getBitmap();
        ByteArrayOutputStream stream= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,0,stream);
        byte[] imagenByte= stream.toByteArray();
        return imagenByte;
    }
}
