package com.example.pictoapp_ah.Interfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.example.pictoapp_ah.Interfaces.elegir_categoria.elegir_categoriaActivity;
import com.example.pictoapp_ah.R;

import java.sql.Time;

public class seleccion_busqueda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccion_busqueda);
    }
    public void bus_palabra(View view)
    {
        Intent bus_palabra=new Intent(getApplicationContext(), bus_palabra.class);
        startActivity(bus_palabra);
    }
    public void bus_categoria(View view) {

        Intent bus_cat=new Intent(getApplicationContext(), elegir_categoriaActivity.class);
        startActivity(bus_cat);
    }


}
