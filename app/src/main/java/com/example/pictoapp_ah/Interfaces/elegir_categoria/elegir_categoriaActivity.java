package com.example.pictoapp_ah.Interfaces.elegir_categoria;

import android.net.Uri;
import android.os.Bundle;

import com.example.pictoapp_ah.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.example.pictoapp_ah.Interfaces.*;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.pictoapp_ah.Interfaces.elegir_categoria.ui.main.SectionsPagerAdapter;

public class elegir_categoriaActivity extends AppCompatActivity implements cat_acciones.OnFragmentInteractionListener,
cat_comida.OnFragmentInteractionListener, cat_cuerpo.OnFragmentInteractionListener, cat_emociones.OnFragmentInteractionListener,
cat_familia.OnFragmentInteractionListener, cat_lugares.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_categoria);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);


    }

    @Override
    public void OnFragmentInteraction(Uri uri) {

    }
}