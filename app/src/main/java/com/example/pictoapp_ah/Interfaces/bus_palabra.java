package com.example.pictoapp_ah.Interfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.R;

import java.io.ByteArrayInputStream;

public class bus_palabra extends AppCompatActivity {

    EditText busqueda;
    //ImageView res_bus;
    TextView resultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_palabra);
        busqueda=(EditText) findViewById(R.id.busqueda);
        resultado=(TextView) findViewById(R.id.no_encont);
        //res_bus=findViewById(R.id.res_ima);
    }

    @Override
    protected void onPause() {
        super.onPause();
        busqueda.setText("");
        //res_bus.setVisibility(View.INVISIBLE);
        resultado.setVisibility(View.INVISIBLE);
    }

    public void buscar(View view)
    {
        //res_bus.setVisibility(View.INVISIBLE);
        resultado.setVisibility(View.INVISIBLE);
        if(busqueda.getText().toString().equals(""))
        {
            resultado.setVisibility(View.VISIBLE);
            resultado.setText("Escribe una palabra, por favor \uD83D\uDE0A");

        }
        else
        {
            byte [] imgRecuperada;
            PictogramasBdHepler bd= new PictogramasBdHepler(this, "pictogramas", null, 1);
            Cursor resultado_consulta=bd.consultaPictoNombre(busqueda.getText().toString());
            if(resultado_consulta.moveToFirst())
            {
                //res_bus.setVisibility(View.VISIBLE);
                imgRecuperada = resultado_consulta.getBlob(0);

                Intent mostrar_a=new Intent(getApplicationContext(), mostrar2.class);
                mostrar_a.putExtra("nombre", busqueda.getText().toString());
                mostrar_a.putExtra("imagen", imgRecuperada);
                startActivity(mostrar_a);
            }
            else
            {
                resultado.setVisibility(View.VISIBLE);
                resultado.setText("☹ No pude encontrar algún pictograma\n relacionado con "+busqueda.getText().toString());
            }
        }
        busqueda.setText("");
    }
    public void selec_busqueda(View view)
    {
        Intent regresar= new Intent(this, seleccion_busqueda.class);
        startActivity(regresar);
    }
}
