package com.example.pictoapp_ah.Interfaces;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.pictoapp_ah.Interfaces.Controlador.Borrar;
import com.example.pictoapp_ah.Interfaces.Controlador.CambioPwd;
import com.example.pictoapp_ah.Interfaces.Controlador.Registrar;
import com.example.pictoapp_ah.Interfaces.elegir_categoria.elegir_categoriaActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.example.pictoapp_ah.Interfaces.ui.main.SectionsPagerAdapter;
import com.example.pictoapp_ah.R;

public class AdministradorActivity extends AppCompatActivity implements Registrar.OnFragmentInteractionListener, Borrar.OnFragmentInteractionListener,
        CambioPwd.OnFragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_administrador);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
        tabs.setupWithViewPager(viewPager);


    }
    @Override
    public void OnFragmentInteraction(Uri uri){}

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);
        if(menuItem.getItemId() == R.id.menuPictos){
            Intent bus_cat=new Intent(getApplicationContext(), elegir_categoriaActivity.class);
            startActivity(bus_cat);
        }if(menuItem.getItemId()==R.id.menuSalir){
            Toast.makeText(this, "Adios!", Toast.LENGTH_SHORT).show();
            finish();
        }
        return false;
    }
}