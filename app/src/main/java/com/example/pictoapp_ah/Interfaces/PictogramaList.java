package com.example.pictoapp_ah.Interfaces;

import android.app.AppComponentFactory;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.GridView;
import com.example.pictoapp_ah.BD.baseContract;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.pictoapp_ah.BD.PictogramasBdHepler;
import com.example.pictoapp_ah.R;

import java.util.ArrayList;

public class PictogramaList extends AppCompatActivity
{
    GridView gridView;
    ArrayList<Pictogramas> list;
    PictogramasListAdapter adapter=null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lugares_list_activity);

        gridView=(GridView)findViewById(R.id.gridView);
        list=new ArrayList<>();
        adapter=new PictogramasListAdapter(this, R.layout.pictogramas_items, list);
        gridView.setAdapter(adapter);


        //Obtener Datos
        PictogramasBdHepler bd= new PictogramasBdHepler(this, "pictogramas", null, 1);
        Cursor cursor=bd.consultaPicto(2);
        list.clear();
        cursor.moveToFirst();
        while (cursor.moveToNext())
        {
            String nom=cursor.getString(0);
            String cat="Lugares";
            String subCat=cursor.getString(3);
            int col_img=cursor.getColumnIndex(baseContract.PictogramasEntry.IMAGEN);
            byte[] image=cursor.getBlob(col_img);
            list.add(new Pictogramas(nom, cat, subCat, image));
        }
        adapter.notifyDataSetChanged();
    }
}
