package com.example.pictoapp_ah.Interfaces.elegir_categoria.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.pictoapp_ah.Interfaces.cat_acciones;
import com.example.pictoapp_ah.Interfaces.cat_comida;
import com.example.pictoapp_ah.Interfaces.cat_cuerpo;
import com.example.pictoapp_ah.Interfaces.cat_emociones;
import com.example.pictoapp_ah.Interfaces.cat_familia;
import com.example.pictoapp_ah.Interfaces.cat_lugares;
import com.example.pictoapp_ah.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    public static Fragment newInstance(int index) {
        Fragment fragment=null;
        switch (index)
        {
            case 1: fragment = new cat_acciones();break;
            case 2: fragment = new cat_comida();break;
            case 3: fragment = new cat_cuerpo();break;
            case 4: fragment = new cat_emociones();break;
            case 5: fragment = new cat_familia();break;
            case 6: fragment = new cat_lugares();break;

        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_elegir_categoria, container, false);
        final TextView textView = root.findViewById(R.id.section_label);
        pageViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}