package com.example.pictoapp_ah.Utilidades;

public class Categorias {
    private  Integer ID;
    private String nombre;

    public Categorias(Integer id, String nom){
        this.ID=id;
        this.nombre=nom;
    }
    public Categorias(){


    }
    public Integer getId(){return ID;}
    public void setID(Integer id){this.ID=id;}

    public String getNombre(){return nombre;}
    public void setNombre(String Nom){this.nombre=Nom;}

}
